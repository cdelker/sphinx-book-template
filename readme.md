# Template for writing a book with Sphinx and Jupyter.

This template can be used for generating a textbook using Sphinx with content written in Jupyter notebooks.
See chapters/Chapter1.ipynb for examples of how to format math, references, and citations.

Files to edit:

- conf.py: Title, author, copyright statement, etc.
- _templates/cover.png: put cover graphic here.
- index.rst: List all the chapter jupyter notebooks or rst files here.


Things that don't work yet due to Sphinx limitations:

- No way to change chapter numbering of appendices to A, B, C, etc.


## Setup

The template requires these Python packages (all can be installed from conda-forge channel)

- sphinx
- sphinxcontrib-bibtex
- nbsphinx
- jupyter
