.. raw:: latex

   \cleardoublepage
   \begingroup
   \renewcommand\chapter[1]{\endgroup}
   \phantomsection


Bibliography
============

.. bibliography:: biblio.bib
   :style: unsrt
