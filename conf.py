# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Book Template'
copyright = '2020, Collin J. Delker'
author = 'Collin J. Delker'
url = 'https://www.collindelker.com'
version = '1'

numfig = True


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
        'nbsphinx',
        'sphinx.ext.imgmath',
        'sphinxcontrib.bibtex',
        # 'sphinxcontrib.cairosvgconverter',  # To convert SVG cell outputs to PDF for Latex    
        'sphinx.ext.mathjax'  # For html
]

nbsphinx_execute_arguments = [
    "--InlineBackend.figure_formats={'svg', 'pdf'}",
    "--InlineBackend.rc={'figure.dpi': 96}",
]

# Remove the 'In [1]' prompts.
nbsphinx_input_prompt = ''
nbsphinx_output_prompt = ''

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', '_backup', 'Thumbs.db', '.DS_Store', '**.ipynb_checkpoints']

# -- EPUB Options ------------------------------------------------------------

epub_title = project
epub_author = author
epub_copyright = copyright
#epub_identifier = '' # ISBN, URL, etc
#epub_scheme = ## 'ISBN', 'URL', etc
#epub_uid = 
epub_cover = ('_static/cover.png', 'cover.html')


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# -- LATEX options ------------------------------------------------------------

latex_engine = 'xelatex'
latex_docclass = {'manual': 'book', 'howto': 'book'}
latex_documents = [('index', 'bookname.tex', project, author, 'manual')]

preamble = r'''
\usepackage[titles]{tocloft}
\cftsetpnumwidth {1.25cm}\cftsetrmarg{1.5cm}
\setlength{\cftchapnumwidth}{0.75cm}
\setlength{\cftsecindent}{\cftchapnumwidth}
\setlength{\cftsecnumwidth}{1.25cm}

\newcommand{\sphinxbackoftitlepage}{%
\vspace*{\fill}
\begin{center}''' + project + r'''
\end{center}
''' + copyright + r'''
\\~\\
\begingroup
\centering
\url{''' + url + r'''}
\\~\\
\endgroup
\vspace*{\fill}
Cover Image: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
}
'''

latex_maketitle = r'''
\begin{titlepage}
\centering \Huge ''' + project + r''' \par
\centering \Large ''' + author + r''' \par
\vspace{1in}
\includegraphics[width=\linewidth]{cover.png}
\end{titlepage}
\sphinxbackoftitlepage
'''

latex_additional_files = ['cover.png']

latex_elements = {
    'fontpkg': r'''
\setmainfont{DejaVu Serif}
\setsansfont{DejaVu Sans}
\setmonofont{DejaVu Sans Mono}
''',
    'preamble': preamble,
    'maketitle': latex_maketitle,
    'fncychap': r'\usepackage[Bjarne]{fncychap}',
    'printindex': r'\footnotesize\raggedright\printindex',
}
