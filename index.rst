.. Book template documentation master file, created by
   sphinx-quickstart on Sun Apr 12 11:51:04 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Book Exammple
=============

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   chapters/Chapter1
   chapters/biblio
